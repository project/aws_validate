<?php

/**
 * @file
 * Provides a module that validates the AWS instance is owned by the current
 * user.
 */

/**
 * Implements hook_menu().
 */
function aws_validate_menu() {
  $items = array();

  $items['admin/config/system/aws-validate'] = array(
    'title' => 'AWS Validate',
    'description' => 'Validate you own the AWS EC2 instance by verifying the Instance ID.',
    'access arguments' => array('aws_validate'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('aws_validate_instanceid_form'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function aws_validate_permission() {
  $return = array();

  $return['aws_validate'] = array(
    'title' => t('Validate AWS Instance'),
    'description' => t('Allow to the user to validate the AWS instance.'),
  );

  return $return;
}

/**
 * Builds the AWS validation form.
 */
function aws_validate_instanceid_form($form, &$form_state) {
  $form['instanceid'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('AWS Instance ID'),
    '#description' => t('Enter the AWS Instance ID of this server.'),
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Validate'),
  );
  return $form;
}

/**
 * Validate callback for the AWS validation form.
 */
function aws_validate_instanceid_form_validate(&$form, &$form_state) {
  $response = drupal_http_request('http://169.254.169.254/latest/meta-data/instance-id');
  $id = trim($form_state['values']['instanceid']);
  if ($response->code != 200 || $id !== $response->data) {
    form_set_error('instanceid', t('The AWS Instance ID does not match.'));
  }
}

/**
 * Submit callback for the AWS validation form.
 */
function aws_validate_instanceid_form_submit(&$form, &$form_state) {
  variable_set('aws_validate_valid', TRUE);
  drupal_set_message('Successfully validated AWS Instance ID');
}
